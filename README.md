# Python style guide of CUED

The is the style guide for the use of Python in teaching at the
Department of Engineering and University of Cambridge. The use of a
style guide has two objectives:

1. The make the learning of Python programming simpler by providing
   consistency of presentation across different parts of the course.
2. To encourage the development of good programming style.
